package com.kata.sg.account.domain.account;

import com.kata.sg.account.core.ValidationException;
import com.kata.sg.account.domain.client.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Calendar;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Class For Testing Acount Operations
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
public class OperationTest {

    /**
     *
     */
    public static final String CLIENT_LAST_NAME= "lastName";
    /**
     *
     */
    public static final String CLIENT_FIRST_NAME= "fistName";
    /**
     *
     */
    public static final String CLIENT_ACCOUNT_NAMBER= "account-1";
    /**
     *
     */
    public static final String EMPTY_ACCOUNT_EXCEPTION ="Operation.account must not be null";

    /**
     *
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given Valid Operation Data Should Create Operation")
    void given_valid_operation_data_should_create_operation() {
        //given
        Client client = new Client(1,CLIENT_LAST_NAME,CLIENT_FIRST_NAME);
        Account account = new Account(1,CLIENT_ACCOUNT_NAMBER,client);
        //when and then
        assertDoesNotThrow(() -> new Operation(
                LocalDateTime.now(),
                OpertaionType.DEPOSIT,
                10,
                account));
    }

    /**
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given not Valid Operation Data Should Create Operation")
    void given_not_valid_operation_data_should_create_operation(){
        //given
        // empty account

        //when and then
        assertThatThrownBy(() -> new Operation(
                        LocalDateTime.now(),
                        OpertaionType.DEPOSIT,
                        10,
                        null))
                .isInstanceOf(ValidationException.class)
                .hasMessageContaining(EMPTY_ACCOUNT_EXCEPTION);
    }
}
