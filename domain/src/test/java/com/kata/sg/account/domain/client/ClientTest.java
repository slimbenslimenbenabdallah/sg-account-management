package com.kata.sg.account.domain.client;

import com.kata.sg.account.core.ValidationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

/**
 * Class For Testing Clients
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
class ClientTest {

    /**
     *
     */
    public static final String CLIENT_LAST_NAME= "lastName";
    /**
     *
     */
    public static final String CLIENT_FIRST_NAME= "fistName";
    /**
     *
     */
    public static final String EMPTY_LASTNAME_EXCEPTION ="Client.lastName must not be empty";

    /**
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given Valid Client Data Should Create Client")
    void given_valid_client_data_should_create_client(){
        assertDoesNotThrow(() -> new Client(1, CLIENT_LAST_NAME, CLIENT_FIRST_NAME));
    }

    /**
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given not Valid Client Data Should Create Client")
    void given_not_valid_client_data_should_create_client(){
        assertThatThrownBy(() -> new Client(1, null, CLIENT_FIRST_NAME))
                .isInstanceOf(ValidationException.class)
                .hasMessageContaining(EMPTY_LASTNAME_EXCEPTION);
    }
}
