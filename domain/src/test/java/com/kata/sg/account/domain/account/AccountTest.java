package com.kata.sg.account.domain.account;

import com.kata.sg.account.core.ValidationException;
import com.kata.sg.account.domain.client.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;


/**
 * Class For Testing Accounts
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
public class AccountTest {
    /**
     *
     */
    public static final String CLIENT_LAST_NAME= "lastName";
    /**
     *
     */
    public static final String CLIENT_FIRST_NAME= "fistName";
    /**
     *
     */
    public static final String CLIENT_ACCOUNT_NAMBER= "account-1";
    /**
     *
     */
    public static final String EMPTY_CLIENT_EXCEPTION ="Account.client must not be null";


    /**
     *
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given Valid Account Data Should Create Account")
    void given_valid_account_data_should_create_account() {
        //given
        Client client = new Client(1,CLIENT_LAST_NAME,CLIENT_FIRST_NAME);
        //when and then
        assertDoesNotThrow(() -> new Account(1, CLIENT_ACCOUNT_NAMBER, client));
    }

    /**
     * @throws ValidationException
     */
    @Test
    @DisplayName("Given not Valid Account Data Should Create Account")
    void given_not_valid_account_data_should_create_account(){
        //given
        //empty Client

        //when and then
        assertThatThrownBy(() -> new Account(1, CLIENT_ACCOUNT_NAMBER, null))
                .isInstanceOf(ValidationException.class)
                .hasMessageContaining(EMPTY_CLIENT_EXCEPTION);
    }
}
