package com.kata.sg.account.domain;

import com.kata.sg.account.domain.account.Account;
import com.kata.sg.account.domain.account.Operation;
import com.kata.sg.account.domain.account.OpertaionType;
import com.kata.sg.account.domain.client.Client;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Class For Testing Account Management process
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
public class AccountManagementTest {

    /**
     *
     */
    public static final String ACCOUNT_HISTORY = "Account(id=1, number=account-1, balance=45.0, client=Client(id=1, lastName=lastName, firstName=fistName), operations=[Operation(date=2022-04-09T08:00, type=DEPOSIT, amount=30.0, currentBalance=55.0), Operation(date=2022-04-10T08:00, type=WITHDRAWAL, amount=10.0, currentBalance=45.0)])";
    /**
     *
     */
    public static final String CLIENT_LAST_NAME= "lastName";
    /**
     *
     */
    public static final String CLIENT_FIRST_NAME= "fistName";
    /**
     *
     */
    public static final String CLIENT_ACCOUNT_NAMBER= "account-1";
    /**
     *
     */
    public static final String dateOperation1 = "2022-04-09 08:00";
    /**
     *
     */
    public static final String dateOperation2 = "2022-04-10 08:00";


    /**
     *
     */
    @Test
    @DisplayName("Should Increase Balance When Deposit Is Made")
    void Should_Increase_Balance_When_Deposit_Is_Made(){

        //Given
        Client client = new Client(1,CLIENT_LAST_NAME,CLIENT_FIRST_NAME);
        Account account = new Account(1,CLIENT_ACCOUNT_NAMBER,client);
        client.setAccount(account);

        //When
        Operation depositOperation = new Operation(
                LocalDateTime.now(),
                OpertaionType.DEPOSIT,
                10,
                account);

        account.addOperation(depositOperation);

        //Then
        assertThat(account.getBalance()).isEqualTo(10);

    }

    /**
     *
     */
    @Test
    @DisplayName("Should Decrease Balance When Withdrawal Is Made")
    void Should_Decrease_Balance_When_Withdrawal_Is_Made(){

        //Given
        Client client = new Client(1,CLIENT_LAST_NAME,CLIENT_FIRST_NAME);
        Account account = new Account(1,CLIENT_ACCOUNT_NAMBER,client);
        account.setBalance(25);
        client.setAccount(account);

        //When
        Operation withdrawalOperation = new Operation(
                LocalDateTime.now(),
                OpertaionType.WITHDRAWAL,
                10,
                account);

        account.addOperation(withdrawalOperation);

        //Then
        assertThat(account.getBalance()).isEqualTo(15);
    }

    /**
     *
     */
    @Test
    @DisplayName("Should display Account history operations")
    void Should_display_Account_history_operations(){

        //Given
        Client client = new Client(1,CLIENT_LAST_NAME,CLIENT_FIRST_NAME);
        Account account = new Account(1,CLIENT_ACCOUNT_NAMBER,client);
        account.setBalance(25);
        client.setAccount(account);
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


        assertThat(account.getBalance()).isEqualTo(25);

        //When
        account.addOperation(new Operation(
                LocalDateTime.parse(dateOperation1,df),
                OpertaionType.DEPOSIT,
                30,
                account));

        account.addOperation(new Operation(
                LocalDateTime.parse(dateOperation2,df),
                OpertaionType.WITHDRAWAL,
                10,
                account));


        //Then
        assertThat(account.getOperations()).hasSize(2);
        assertThat(account.getBalance()).isEqualTo(45);
        assertThat(account.toString()).isEqualTo(ACCOUNT_HISTORY);
    }


}
