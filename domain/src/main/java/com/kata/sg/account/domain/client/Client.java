package com.kata.sg.account.domain.client;

import com.kata.sg.account.core.Validator;
import com.kata.sg.account.domain.account.Account;
import lombok.*;
import javax.validation.constraints.NotEmpty;

/**
 * Class For Clients
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
@Data
@ToString(exclude = {"account"})
@NoArgsConstructor
public class Client {

    /**
     * The Client Id
     */
    private long id;

    /**
     * The Client LastName
     */
    @NotEmpty
    private String lastName;

    /**
     * The Client FirstName
     */
    @NotEmpty
    private String firstName;

    /**
     * The Account's Client
     */
    private Account account;

    /**
     *
     * @param id
     * @param lastName
     * @param firstName
     */
    public Client(long id, String lastName, String firstName) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;

        Validator.validate(this);
    }

}
