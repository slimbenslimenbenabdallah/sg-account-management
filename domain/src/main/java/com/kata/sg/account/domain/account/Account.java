package com.kata.sg.account.domain.account;

import com.kata.sg.account.core.ValidationException;
import com.kata.sg.account.core.Validator;
import com.kata.sg.account.domain.client.Client;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Class For Accounts
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
@Data
@NoArgsConstructor
@ToString
public class Account {

    /**
     * The Account Id
     */
    @NotNull
    private long id;

    /**
     * The Account Number
     */
    @NotEmpty
    private String number;

    /**
     * The Account balance
     */
    private double balance = 0;

    /**
     * The Account's Client
     */
    @NotNull
    private Client client;

    /**
     * The Account operations
     */
    private List<Operation> operations = new ArrayList<>();

    /**
     *
     * @param id
     * @param number
     * @param client
     */
    public Account(long id, String number, Client client) {
        this.id = id;
        this.number = number;
        this.client = client;

        Validator.validate(this);
    }


    /**
     * method for adding Operations
     * @param operation
     */
    public void addOperation(Operation operation){
        this.operations.add(operation);
        switch (operation.getType()){
            case DEPOSIT: this.balance+= operation.getAmount(); break;
            case WITHDRAWAL: this.balance -= operation.getAmount();break;
            default:
                throw new ValidationException("Unrecognized Operation");
        }

        operation.setCurrentBalance(this.balance);
    }


}
