package com.kata.sg.account.core;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import java.util.Locale;
import java.util.Set;

public class Validator {

    private static final javax.validation.Validator JAVAX_VALDATOR;

    static {
        Locale.setDefault(Locale.ENGLISH);
        JAVAX_VALDATOR = Validation.buildDefaultValidatorFactory().getValidator();
    }
    public static <T> T validate(T bean) throws ValidationException {
        Set<ConstraintViolation<T>> constraintViolations = JAVAX_VALDATOR.validate(bean);
        if(!constraintViolations.isEmpty()){
            throw new ValidationException(buildMessageException(constraintViolations));
        }
        return bean;
    }

    private static <T> String buildMessageException(Set<ConstraintViolation<T>> constraintViolations){
        StringBuilder sb = new StringBuilder();
        constraintViolations.forEach( c -> {
            sb.append(c.getRootBeanClass().getSimpleName());
            sb.append(".");
            sb.append(c.getPropertyPath());
            sb.append(" ");
            sb.append(c.getMessage());
            sb.append(System.getProperty("line.separator"));
        });
        return sb.toString();
    }
}
