package com.kata.sg.account.domain.account;

import com.kata.sg.account.core.Validator;
import lombok.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class For Operations
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
@Data
@NoArgsConstructor
@ToString(exclude = {"account"})
public class Operation {

    /**
     * The Operation Date
     */
    @NotNull
    private LocalDateTime date;

    /**
     * The Operation Type
     */
    @NotNull
    private OpertaionType type;

    /**
     * the Operation Amount
     */
    private double amount = 0;

    /**
     * the new Account Balance
     */
    private double currentBalance = 0;

    /**
     * the Operation's Account
     */
    @NotNull
    private Account account;

    /**
     *
     * @param date
     * @param type
     * @param amount
     * @param account
     */
    public Operation(LocalDateTime date, OpertaionType type, double amount, Account account) {
        this.date = date;
        this.type = type;
        this.amount = amount;
        this.account = account;

        Validator.validate(this);
    }
}
