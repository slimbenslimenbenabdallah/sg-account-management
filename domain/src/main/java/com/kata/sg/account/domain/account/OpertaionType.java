package com.kata.sg.account.domain.account;

/**
 * Enum For OpertaionTypes
 * @author salim.benabdallah@storm-group.fr - Apr 10, 2022
 */
public enum OpertaionType {
    /**
     *
     */
    DEPOSIT,

    /**
     *
     */
    WITHDRAWAL
}
