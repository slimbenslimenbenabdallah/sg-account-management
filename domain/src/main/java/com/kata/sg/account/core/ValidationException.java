package com.kata.sg.account.core;

public class ValidationException extends RuntimeException{

    public ValidationException(String message){
        super(message);
    }
}
