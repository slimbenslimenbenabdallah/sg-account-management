# account-management

## Getting started
This project is a kata exam program for SG Bank,it present the domain
of a Bank account management for DEPOSIT & WITHDRAWAL operations

## Test and quality management tool
- JACOCO maven plugin

## Build Project
- mvn clean verify

## Technical documentation
- javadoc report path: /target/apidocs/index.html

## Test management document
- tests report path: /target/site/index.html

